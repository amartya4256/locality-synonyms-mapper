from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib, ssl
from config import Config

def mailer(path, email):
    print('YOU ARE INSIDE.')
    conf = Config
    subject = "Your Locality - Synonym mapping is ready."
    body = "Please download the file in the attachment."

    message = MIMEMultipart()
    message["From"] = conf.MAIL_USERNAME
    message["To"] = email
    message["Subject"] = subject

    message.attach(MIMEText(body, "plain"))

    filename = path

    with open(filename, "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())
    encoders.encode_base64(part)

    part.add_header(
        "Content-Disposition",
        "attachment; filename= mapped_data.csv")

    message.attach(part)
    text = message.as_string()

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(conf.MAIL_USERNAME, conf.MAIL_PASSWORD)
        server.sendmail(conf.MAIL_USERNAME, email, text)