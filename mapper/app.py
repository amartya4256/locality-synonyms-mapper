from flask import Flask, render_template, request,send_file
from werkzeug import secure_filename
from config import Config
import os
from map import *
from multiprocessing import Process

conf = Config
app = Flask(__name__)
app.config.from_object(conf)

@app.route('/')
def upload():
    return render_template('upload.html')


@app.route('/upload', methods = ['GET', 'POST'])
def mapper():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'No file uploaded.'

        f = request.files['file']
        email = request.form['email']
        print(email)
        if email == '':
            return 'You must enter an email ID.'

        if f.filename == '':
            return 'No file uploaded.'
        f.save(os.path.join(app.config['DATA'], secure_filename(f.filename)))
        y = secure_filename(f.filename)
        file_path = 'static/files/'+secure_filename(f.filename)
        p1 = Process(target=mapping, args=(file_path, y, 1, email))
        p1.start()
        p2 = Process(target=mapping, args=(file_path, y, 2, email))
        p2.start()
        p3 = Process(target=mapping, args=(file_path, y, 3, email))
        p3.start()
        p4 = Process(target=mapping, args=(file_path, y, 4, email))
        p4.start()
        #path = 'static/mapped/'+secure_filename(f.filename)
        #return send_file(path, as_attachment=True)
        return 'The file has been uploaded and being mapped.'

if __name__ == '__main__':
    app.run()