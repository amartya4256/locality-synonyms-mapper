import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
import os
from config import Config
from mail import mailer

conf = Config

def mapping(x, y, n, email):

    data = pd.read_csv(x)
    if(len(data)>=4):
        if n == 1:
            data = data[:int(0.25 * len(data))]
        if n == 2:
            data = data[int(0.25 * len(data)):int(0.5 * len(data))]
        if n == 3:
            data = data[int(0.5 * len(data)):int(0.75 * len(data))]
        if n == 4:
            data = data[int(0.75 * len(data)):]

    data = data[['Locality', 'city']]
    all_data = pd.read_csv('static/ner_cleaned2.csv')
    data.replace(np.nan, '', inplace=True)
    all_data.replace(np.nan, '', inplace = True)
    data.drop_duplicates(inplace = True)
    data.reset_index(inplace = True)
    new_data = pd.DataFrame(columns = ['Locality', 'synonyms', 'city'])
    for i in range(len(data)):
        print(i * 100 / len(data), '%')
        for j in range(len(all_data)):
            a = fuzz.WRatio(data['Locality'][i], all_data['locality'][j])
            if a > 90:
                new_data = new_data.append({'Locality' : data['Locality'][i], 'synonyms' : all_data['locality'][j], 'city' : data['city'][i]}, ignore_index = True)

    path = 'static/mapped/'+y+'_'+str(n)
    new_data.to_csv(path)
    try:
        print('process ', n, ' is trying.')
        new_path = 'static/mapped/'+y+'_'
        data1 = pd.read_csv(new_path+'1')
        data2 = pd.read_csv(new_path+'2')
        data3 = pd.read_csv(new_path+'3')
        data4 = pd.read_csv(new_path+'4')
        final_data = data1.append(data2).append(data3).append(data4)
        final_data.drop_duplicates(inplace=True)
        final_data.reset_index(inplace=True)
        final_data.drop(['index', 'Unnamed: 0'], axis=1, inplace=True)
        exp = 'static/mapped/'+y
        final_data.to_csv(exp)
        os.remove(new_path+'1')
        os.remove(new_path+'2')
        os.remove(new_path+'3')
        os.remove(new_path+'4')

        mailer(exp, email)
        print('process ', n, ' made it alive.')



    except:
        print('process ', n, ' is done.' )